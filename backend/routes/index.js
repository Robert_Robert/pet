var express = require('express');
var router = express.Router();
let MainController = require('../controllers/MainController.js');


MainController = new MainController();

/* GET home page. */
router.get('/', function(req, res, next) {
	return MainController.index(req, res, next);
});

module.exports = router;
