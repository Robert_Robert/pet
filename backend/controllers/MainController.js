let mysql = require('../options/mysql');

class MainController {

  constructor() {
    this.mysql = mysql;
    this.mysql.connect();
  }

  index(req, res, next) {
    res.render('index', { title: 'Express' });
  }
}

module.exports = MainController;