import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "./counterSlice.tsx";

export default configureStore({
  reducer: {
    counter: counterReducer,
  },
});
