import { connect } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { increment, decrement } from "../store/counterSlice.tsx";

interface ComponentOwnProps {
  prop1?: string;
  prop2?: number;
}

interface StateProps {
  counter: number;
}

const MapStateToProps = (state, ownProps?: ComponentOwnProps): StateProps => {
  const { counter } = state;

  return { counter: counter.value as number };
};

// The second variant MapStateToProps
// function MapStateToProps(state) {
// return;
// }

// The first variant MapDispatchToProps (function)
// Gives more flexibility. Gain access to dispatch and ownProps
// will reinvoke in case ownProps defined
const MapDispatchToPtops = (dispatch, ownProps?: ComponentOwnProps) => {
  return {
    // increment: dispatch({ type: 'increment' })
    increment: dispatch(increment()),
    decrement: dispatch(decrement()),
    // onClick: (event: MouseEvent) => dispatch(increment('some data'))   pas data to reducer
  };
};

export const MapStateComponent = (props) => {
  let params = useParams();

  return (
    <section>
      <h1>MapStateToProps. counter {props.counter}</h1>
      <button onClick={() => props.dispatch(increment())}>Increase</button>
      {/* <button onClick={props.onClick}>Increase</button> */}
      <div>
        <Link to="/">Main Page default link</Link>
      </div>
      <div>URL param - {params.reduxId}</div>
    </section>
  );
};

// in case mapDispatchToProps waasn't provided as second param, the dispatch method will
// automatically situate in props object, but if presented - dispatch function won't
// be available anymore
// export default connect(null, MapDispatchToPtops)(MapStateComponent);
export default connect(MapStateToProps, MapDispatchToPtops)(MapStateComponent);
