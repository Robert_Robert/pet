import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import logo from "./logo.svg";
import "./App.css";

function App() {
  // State hooks
  const [counter, changeState] = useState(0);
  const [fruit, changeFruitState] = useState("banana");

  // Effects hook: componentDidMount componentDidUpdate componentWillUnmount
  // By default, React runs the effects after every render — including the first render.
  useEffect(() => {
    // Update the document title using the browser API
    document.title = `You clicked ${counter} times`;

    // The arrow func. will execute before component unmount and before next rerender
    return () => {};
  });

  const renderer = (
    <div className="App">
      <header className="App-header">
        <button onClick={() => changeState(counter + 1)}>More</button>
        <button onClick={() => changeState(counter - 1)}>Less</button>
        <h1>Counter: {counter}</h1>

        <h1>Fruit: {fruit}</h1>
        <button onClick={() => changeFruitState("Orange")}>Orange</button>
        <button onClick={() => changeFruitState("Pineapple")}>Pineapple</button>

        <div>
          <Link to={`/redux/2`} key={2}>
            Redux comp. Page default link
          </Link>
        </div>
      </header>
    </div>
  );

  console.log(renderer);

  return renderer;
}

export default App;
